![Open Source Sweden](/osslogo.png)

## Hello
Open Source Sweden is an industry association that supports the interests of Swedish Open Source companies.

The English version of this website is currently limited, in the sense that the current pages are roughly translated. If you have any problems understanding us, contact kansli@opensourcesweden.org for help. We will work to improve the state of the english speaking site in the future.
