# Styrelsen
Föreningens styrelse består efter föreningstämma 2023 av:

## Ordförande
```
Björn Lundell, Forskare vid Högskolan i Skövde, bjorn.lundell@his.se
```

## Vice Ordförande
```
Tony Nicolaides, vice ordf, RedPill LinPro, tony.nicolaides@redpillinpro.se
```

## Styrelsemedlemmar
```
Tomas Gustavsson, Keyfactor, tomas@keyfactor.se
Jonas Feist, Redbridge, jfeist@redbridge.se
Magnus Glantz, Red Hat AB, sudo@redhat.com
Mathias Lindroth, ACF Legal Intl AB, m@acf.legal
Erik Lönroth, Dwellir AB, erik.lonroth@Dwellir.se
Jonas Larsson, Frontwalker AB, jonas.larsson@frontwalker.se
```

## Kassör
```
Jonas Feist, Redbridge, jfeist@redbridge.se
```

## Suppleanter
```
Colin Campbell, Digitalist, colin.campbell@digitalist.se
Johan Bernhardsson,Kafit AB, johan@kafit.se 
```

## Valberedning 
```
Jonas Larsson, Frontwalker AB, jonas.larsson@frontwalker.se
Jonas Feist, vice ordf, Redbrigde AB, onas.feist@redbridge.se
```

## Revisorer
```
Malena Wegin, Revidens Konsult AB 
```
